package com.example.amartineza.practicarecursos;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amartineza on 3/1/2018.
 */

public class FormAdapter extends RecyclerView.Adapter<FormAdapter.ItemViewHolder>{
    private Context mContexto;
    private List<FormModel> mList;

    public FormAdapter(Context mContexto, List<FormModel> mList) {
        this.mContexto = mContexto;
        this.mList = mList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_form, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final FormModel model = mList.get(position);

        holder.tvUserName.setText(model.getUserName());
        holder.tvUserAge.setText(model.getUserAge());
        holder.tvUserAddres.setText(model.getUserAddress());
        holder.tvUserSex.setText(model.getUserSex());
        if(model.getUserSex()=="Femenino"){
            holder.ivUserPhoto.setImageResource(R.drawable.mujer);
        }else{
            holder.ivUserPhoto.setImageResource(R.drawable.hombre);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView ivUserPhoto;
        TextView tvUserName;
        TextView tvUserAge;
        TextView tvUserAddres;
        TextView tvUserSex;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ivUserPhoto = (ImageView)itemView.findViewById(R.id.user_photo);
            tvUserName = (TextView)itemView.findViewById(R.id.tv_user_name);
            tvUserAge = (TextView)itemView.findViewById(R.id.tv_user_age);
            tvUserAddres = (TextView)itemView.findViewById(R.id.tv_user_address);
            tvUserSex = (TextView)itemView.findViewById(R.id.tv_user_sex);
        }
    }


}
