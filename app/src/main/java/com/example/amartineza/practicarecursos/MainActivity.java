package com.example.amartineza.practicarecursos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    List<FormModel> list = new ArrayList<>();
    private FormAdapter adapter;
    String userName;
    String userAge;
    String userAddress;
    String userSex;

    @BindView(R.id.form_recycler)
    RecyclerView rvForm;

    @BindView(R.id.et_user_name)
    EditText etUserName;

    @BindView(R.id.et_user_age)
    EditText etUserAge;

    @BindView(R.id.rb_female)
    RadioButton rbUserFemale;

    @BindView(R.id.et_user_address)
    EditText etUserAddress;

    @BindView(R.id.rg_users_sex)
    RadioGroup rgUserSex;

    @OnClick(R.id.btn_save)
    public void onClickSave() {
        if(validateForm()){
            userName = etUserName.getText().toString();
            userAge = etUserAge.getText().toString();
            userAddress = etUserAddress.getText().toString();
            FormModel model = new FormModel(userName, userAge, userSex, userAddress);
            list.add(model);
            adapter.notifyDataSetChanged();
            etUserName.setText("");
            etUserAge.setText("");
            etUserAddress.setText("");
            rgUserSex.clearCheck();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        rgUserSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_female:
                        userSex = "Femenino";
                        break;
                    case R.id.rb_male:
                        userSex = "Masculino";
                        break;
                }

            }
        });
        startAdapter();

    }

    public void startAdapter() {
        adapter = new FormAdapter(this, list);
        rvForm.setAdapter(adapter);
        rvForm.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvForm.hasFixedSize(); //siempre tienen el mismo tamaño
    }

    public Boolean validateForm() {
        Boolean itsValid = true;
        if (etUserName.getText().toString().isEmpty()) {
            etUserName.setError("Introduce el Nombre");
            itsValid = false;
        }
        if (etUserAddress.getText().toString().isEmpty()) {
            etUserAddress.setError("Introduce la Direccion");
            itsValid = false;
        }

        if(etUserAge.getText().toString().isEmpty()){
            etUserAge.setError("Introduce tu Edad");
            itsValid = false;
        }

        if(userSex == null){
            Toast.makeText(this, "Selecciona tu Sexo", Toast.LENGTH_LONG).show();
            itsValid = false;
        }

        return itsValid;
    }
}
