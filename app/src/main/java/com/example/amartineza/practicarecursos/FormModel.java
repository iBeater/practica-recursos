package com.example.amartineza.practicarecursos;

/**
 * Created by amartineza on 3/1/2018.
 */

public class FormModel {
    private String userName;
    private String userAge;
    private String userSex;
    private String userAddress;

    public FormModel(String userName, String userAge, String userSex, String userAddress) {
        this.userName = userName;
        this.userAge = userAge;
        this.userSex = userSex;
        this.userAddress = userAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAge() {
        return userAge;
    }

    public void setUserAge(String userAge) {
        this.userAge = userAge;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }
}
